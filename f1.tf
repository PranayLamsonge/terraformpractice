terraform {
  backend "s3" {
    bucket = "terraform54321"
    region = "ap-south-1"
    key = "terraform.tfstate"
  }
}
provider "aws" {
    region = "ap-south-1"
}


resource "aws_instance" "myinstance" {
    ami           = "ami-0763cf792771fe1bd"
    instance_type = var.instance_type
    key_name      = var.key_pair
    tags          = var.tags
    vpc_security_group_ids = var.sg_ids

}

data "aws_security_groups" "mysg" {
    filter {
        name   = "vpc-id"
        values = [var.vpc_id]
    }
  #  filter {
   #     name = "group-name"
      #  values = ["launch-wizard-7"]
    #}
}
