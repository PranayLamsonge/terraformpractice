variable "region" {
    description = "This is my home region"
    default = "ap-south-1"

}
variable "key_pair" {
    description = "my key pair"
    default = "instancekey"
}

variable "instance_type" {
    description = "enter instance type"
    default = "t2.micro"
}

variable "vpc_id" {
    default = "vpc-012816e9cfdbd69fc"  //jis vpc ko security group attch rahega wo vpc denge
}

variable "tags" {
    type = map
    default = {
        name = "myinstance"
        env = "devops"
    }
}

variable "sg_ids"{
    type = list           //security group ka datatype list hota hai wo dena hoga
    default = ["sg-0f24b505900e889ee"]
}